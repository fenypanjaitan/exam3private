import React from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import Login from './component/login/Login';
import Dashboard from './component/dashboard/Dashboard';
import AddCostumer from './component/costumer/AddCostumer';
import ListCostumer from './component/costumer/ListCostumer';
import EditCostumer from './component/costumer/EditCostumer';

function App() {
	return (
		<Switch>
			<Route path="/" exact component={Login} />
			<Route path="/dashboard" exact component={Dashboard} />
			<Route path="/inLaundry" exact component={AddCostumer} />
			<Route path="/dataPesanan" exact component={ListCostumer} />
      <Route path="/editPesanan/:id" component={EditCostumer}/>




		</Switch>
	);
}

export default App;
