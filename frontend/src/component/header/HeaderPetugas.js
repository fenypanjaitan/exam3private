import React, { Component } from 'react'

export class HeaderPetugas extends Component {
    render() {
        return (
            <div>
                <div className="header">
					<a href="/dashboard" className="logo">
						Laundry
					</a>
					<div className="header-right">
						<a href="/dashboard">
							Home
						</a>
						<a href="/dataPesanan">List Pesanan</a>
						<a href="#logout">Logout</a>
					</div>
				</div>
            </div>
        )
    }
}

export default HeaderPetugas
