import React, { Component } from 'react';
import Header from '../header/Header';
import './Login.css';

export class Login extends Component {
	constructor() {
		super();
		this.state = {
			username: '',
			password: '',
		};
    }
    
	onSubmit = (e) => {
		e.preventDefault();
		
	};

	onChange = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};

	login = ()=>{
		if(this.state.username==="laundry" && this.state.password==="password"){
			this.props.history.push('/dashboard')
		}else {
			alert("username atau password salah")
		}
		
	}

	render() {
		return (
			<div>
				<Header />

				<div>
					<form onSubmit={this.onSubmit} className="container">
						<div className="imgcontainer">
							<img
								src="https://icon-library.com/images/avatar-icon/avatar-icon-8.jpg"
								alt="Avatar"
								className="avatar"
							/>
						</div>

						<div className="container">
							<label >
								<b>Username</b>
							</label>
							<input
								type="text"
								name="username"
								onChange={this.onChange}
								placeholder="Enter Username"
								required
							/>

							<label>
								<b>Password</b>
							</label>
							<input
								type="password"
								name="password"
								onChange={this.onChange}
								placeholder="Enter Password"
								required
							/>

							<button type="submit" onClick={() => this.login()}>Login</button>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

export default Login;
