import React, { Component } from 'react'
import './Dashboard.css'
import HeaderPetugas from '../header/HeaderPetugas'
import { Link } from 'react-router-dom'

export class Dashboard extends Component {
    render() {
        return (
            <div>
                <HeaderPetugas/>
                <div className="content">
                    <Link to="/inLaundry"><button className="in"> Laundry Masuk </button></Link>
                    <Link to="/dataPesanan"><button className="out"> Pengambilan Laundry </button></Link>
                </div>
                
            </div>
        )
    }
}

export default Dashboard
