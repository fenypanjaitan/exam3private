import React, { Component } from 'react';
import HeaderPetugas from '../header/HeaderPetugas';
// import { Link } from 'react-router-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';

export class ListCostumer extends Component {
	constructor() {
		super();
		this.state = {
			costumers: []
		};
	}

	componentDidMount() {
		axios
			.get(`http://localhost:8080/costumers`)
			.then((res) => {
				const costumers = res.data;
				this.setState({ costumers });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	delete = (id) => {
		axios
			.delete('http://localhost:8080/delete/' + id)
			.then(() => {
				window.location.reload();
			})
			.catch((error) => {
				console.log(error);
			});
	};

	render() {
		return (
			<div>
				<HeaderPetugas />
				<div className="body">
					<h2>Daftar Pelanggan Laundry</h2>
					<div style={{ overflowX: 'auto' }}>
						<table>
							<thead>
								<tr>
									<th>No</th>
									<th>Kode Pesanan</th>
									<th>Nama Costumer</th>
									<th>Tanggal Masuk</th>
									<th>Tanggal Pengambilan</th>
									<th>Berat</th>
									<th>Total Harga</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								{this.state.costumers.length ? (
									this.state.costumers.map((item, i) => (
										<tr key={i}>
											<td>{i + 1}</td>
											<td>{item.kodePemesanan}</td>
											<td>{item.namaCostumer}</td>
											<td>{item.tglMasuk}</td>
											<td>{item.tglPengambilan}</td>
											<td>{item.berat}</td>
											<td>{item.totalBayar}</td>
											<td>{item.status}</td>
											<td>
												<Link to={'/editPesanan/' + item.id}>
													<button className="btnEdit">Edit</button>{' '}
												</Link>
												<button
													className="btnDelete"
													onClick={() => {
														if (window.confirm('Yakin ingin menghapus data?')) {
															this.delete(item.id);
														}
													}}
												>
													Hapus
												</button>
											</td>
										</tr>
									))
								) : (
									<tr />
								)}
								<tr>
									<td />
									<td />
									<td />
									<td />
									<td />
									<td />
									<td />
									<td />
									<td />
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}

export default ListCostumer;
