import React, { Component } from 'react';
import HeaderPetugas from '../header/HeaderPetugas';
import './Costumer.css';
import axios from 'axios';

export class AddCostumer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			kodePemesanan: '',
			namaCostumer: '',
			tglMasuk: '',
			tglPengambilan: '',
			berat: '',
			harga: 5000,
			status: ''
		};
	}

	handleChange = (event) => {
		this.setState({ [event.target.name]: event.target.value });
	};

	handleSubmit = (tambah) => {
		tambah.preventDefault();
		axios.post('http://localhost:8080/addCostumer', this.state);
		this.props.history.push('/dataPesanan');
		window.location.reload();
	};
	render() {
		return (
			<div>
				<HeaderPetugas />
				<div className="container">
					<h2>Data Costumer</h2>
					<form onSubmit={this.handleSubmit}>
						<div className="row">
							<div className="col-25">
								<label>Kode Pemesanan</label>
							</div>
							<div className="col-75">
								<input
									type="text"
									name="kodePemesanan"
									onChange={this.handleChange}
									placeholder="Masukkan Kode Pemesanan.."
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-25">
								<label>Nama Costumer</label>
							</div>
							<div className="col-75">
								<input
									type="text"
									name="namaCostumer"
									onChange={this.handleChange}
									placeholder="Nama Costumer.."
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-25">
								<label>Tanggal Masuk</label>
							</div>
							<div className="col-75">
								<input
									type="date"
									name="tglMasuk"
									onChange={this.handleChange}
									placeholder="tgl Masuk.."
								/>
							</div>
						</div>

						<div className="row">
							<div className="col-25">
								<label>Tanggal Pengambilan</label>
							</div>
							<div className="col-75">
								<input
									type="date"
									name="tglPengambilan"
									onChange={this.handleChange}
									placeholder="tgl Pengambilan.."
								/>
							</div>
						</div>

						<div className="row">
							<div className="col-25">
								<label>Berat</label>
							</div>
							<div className="col-75">
								<input type="text" name="berat" onChange={this.handleChange} placeholder="berat.." />
							</div>
						</div>

						<div className="row">
							<div className="col-25">
								<label>Harga per Kg</label>
							</div>
							<div className="col-75">
								<select id="harga" name="harga" onChange={this.handleChange}>
									<option value="5000">{this.state.harga}</option>
								</select>
							</div>
						</div>
						<div className="row">
							<div className="col-25">
								<label>Status Pembayaran</label>
							</div>
							<div className="col-75">
								<select id="status" name="status" onChange={this.handleChange}>
									<option value="">--pilih--</option>
									<option value="Lunas">Cash</option>
									<option value="Credit">Bayar di Pengambilan</option>
								</select>
							</div>
						</div>

						<div className="row" style={{ marginTop: 20 }}>
							<input type="submit" value="Submit" />
						</div>
					</form>
				</div>
			</div>
		);
	}
}

export default AddCostumer;
