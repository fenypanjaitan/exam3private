package com.example.demo.controller;

import com.example.demo.entity.Costumer;
import com.example.demo.service.CostumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class CostumerController {
    @Autowired
    private CostumerService service;

    @PostMapping("/addCostumer")
    public Costumer addCostumer(@RequestBody Costumer costumer){
        return service.saveCostumer(costumer);
    }

    @GetMapping("/costumers")
    public List<Costumer> findAllCostumers(){
        return service.getCostumers();
    }
    

    @GetMapping("/costumers/{id}")
    public Costumer findCostumerById(@PathVariable int id){
        return service.getCostumerById(id);
    }

    @GetMapping("/costumer/{namaCostumer}")
    public Costumer findCostumerByNamaCostumer(@PathVariable String namaCostumer){
        return service.getCostumerByNamaCostumer(namaCostumer);
    }

    @GetMapping("/cByKodePemesanan/{kodePemesanan}")
    public Costumer findCostumerByKodePemesanan(@PathVariable String kodePemesanan){
        return service.getCostumerByKodePemesanan(kodePemesanan);
    }

    
    @PutMapping("/update/{id}")
    public Costumer updateCostumer(@RequestBody Costumer costumer, @PathVariable int id){
        costumer.setId(id);
        return service.updateCostumer(costumer);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteCostumer(@PathVariable int id){
        return service.deleteCostumer(id);
    }
}
