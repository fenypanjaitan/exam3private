package com.example.demo.service;

import com.example.demo.entity.Costumer;
import com.example.demo.repository.CostumerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CostumerService {
    @Autowired
    private CostumerRepository repository;

    public Costumer saveCostumer(Costumer costumer){
        return (Costumer) repository.save(costumer);
    }

    public List<Costumer> getCostumers(){
        return repository.findAll();
    }

    public Costumer getCostumerById(int id){
        return repository.findById(id).orElse(null);
    }

    public Costumer getCostumerByKodePemesanan(String kodePemesanan) {
        return repository.findByKodePemesanan(kodePemesanan);
    }

    public Costumer getCostumerByNamaCostumer(String namaCostumer){
        return repository.findByNamaCostumer(namaCostumer);
    }

    public String deleteCostumer(int id){
        repository.deleteById(id);
        return "Costumer dihapus";
    }

    public Double totalHarga(Costumer costumer){
        return costumer.getBerat()*costumer.getHarga();
    }


    public Costumer updateCostumer(Costumer costumer){
        Costumer existingCostumer = (Costumer) repository.findById(costumer.getId()).orElse(null);
        existingCostumer.setKodePemesanan(costumer.getKodePemesanan());
        existingCostumer.setNamaCostumer(costumer.getNamaCostumer());
        existingCostumer.setTglMasuk(costumer.getTglMasuk());
        existingCostumer.setTglPengambilan(costumer.getTglPengambilan());
        existingCostumer.setBerat(costumer.getBerat());
        existingCostumer.setHarga(costumer.getHarga());
        existingCostumer.setTotalBayar(costumer.getTotalBayar());
        existingCostumer.setStatus(costumer.getStatus());

        return (Costumer) repository.save(existingCostumer);
    }


}
